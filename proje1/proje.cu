#include "cuda_runtime.h"
#include "device_launch_parameters.h"
# include <limits.h>
# include <string.h>
#include <stdio.h>
#include<iostream>
#include<fstream>
#include <sstream>
#include <stdlib.h>
#include <math.h>
#include <cuda.h>
#include <chrono>
#include <vector>
#include <math.h>
#include <time.h>


using namespace std;
#define MAX_THREADS_PER_BLOCK 512

int no_of_nodes=10;
int edge_list_size=10;
int  f = 0, r = -1;

#define N 100
int *a;
int *transpose;

double b = 0;


int main()
{
	
	int num_of_blocks = 3;
	int num_of_threads_per_block = num_of_blocks;
	
	if (num_of_blocks>MAX_THREADS_PER_BLOCK)
	{
		num_of_blocks = (int)ceil(num_of_blocks / (double)MAX_THREADS_PER_BLOCK);
		num_of_threads_per_block = MAX_THREADS_PER_BLOCK;
	}

	
	a = (int *)malloc(N * N * sizeof(int));
	transpose = (int *)malloc(N * N * sizeof(int));
	
	bool stop;
	stop = false;
	dim3  grid(num_of_blocks, 1, 1);
	dim3  threads(num_of_threads_per_block, 1, 1);
	
	

	
	///////////////////////////////////////////////////////////////
	auto start = chrono::steady_clock::now();

	srand(time(NULL));
	
	int  i, j;
	
	cout << "The matrix is:" << endl;
	for (i = 0; i < N; ++i) {
		for (j = 0; j < N; ++j)
		{
			*(a + i*N + j) = round(rand()/1000);
			cout << *(a + i*N + j) << " ";
		}
		
		cout << endl;
	}
	cout << endl;
	for (i = 0; i<N; ++i)
	for (j = 0; j<N; ++j) {
		*(transpose + i*N + j) = *(a + j*N + i);
	}
	cout << "The transpose of the matrix is:" << endl;
	for (i = 0; i<N; ++i) {
		for (j = 0; j<N; ++j)
			cout << *(transpose + i*N + j) << " ";
		cout << endl;
	}
	
	
			
	auto end = chrono::steady_clock::now();
	cout << "\nElapsed time in milliseconds : "
		<< chrono::duration_cast<chrono::milliseconds>(end - start).count()
		<< " ms" << endl;
	getchar();

	
	////////////////////////////////////////////////////////////////

	free(a);
	free(transpose);
	
		
	return 0;
}
